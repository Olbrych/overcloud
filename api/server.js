"use strict";
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
var jwt = require('jsonwebtoken');
var passport = require('passport');


var Api = require('./app/api');
var Post = require('./app/models/Post');
var User = require('./app/models/User');
app.set('superSecret', Api.config.secret);

mongoose.connect(Api.config.mongodb, {}, (err) => {
    if (err)
        console.log(`Mongodb error ${err}`);
});


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(passport.initialize());

var passportProvider = require('./app/passport');
passportProvider(passport);


var port = process.env.PORT || 8080;
var router = express.Router();



router.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
   // console.log('Routing is working.');
    next();
});



router.get('/', (req, res) => res.json({message: "Welcome OverCloud. Let's Start. "}));



router.route('/post')
        .post(
                passport.authenticate('jwt', {session: false}),
                function (req, res) {
                    
                    let post = new Post();

                    post.title = req.body.title;
                    post.text = req.body.text;
                    post.created_at = Date.now();
                    console.log(req.user);
                    post.user = req.user._id;
                    post.save(err => Api.callback(res, err, post));
                })
        .get((req, res) => {
            console.log(req.query);
            Api.queryBuilder(Post, res, req.query)
         });

router.route('/post/:id')
        .get(function (req, res) {
            Post.findById(req.params.id, (err, object) => Api.callback(res, err, object));
        })
        .put(passport.authenticate('jwt', {session: false}), function (req, res) {
            
            Post.findOne({_id: req.params.id, user: req.user._id}, (err, object) => {
                if (err || !object) {
                    Api.callback(res, err || {'error':'object not found'}, null);
                }
                
                object.title = req.body.title;
                object.text = req.body.text;
                object.save(err => Api.callback(res, err, object));
            });
        })
        .delete(passport.authenticate('jwt', {session: false}),function (req, res) {
            Post.findOne({_id: req.params.id, user: req.user._id}, (err, object) => {
                if (err || !object) {
                    Api.callback(res, err || {'error':'object not found'}, null);
                }else{
                    object.remove();
                    Api.callback(res, err, {'message': `Deleted ${req.params.id}`});
                }
            });
        //    Post.remove({'_id': req.params.id}, (err, object) => Api.callback(res, err, {'message': `Deleted ${req.params.id}`}));
        });

router.route('/auth')
        .post(function (req, res) {
            User.findOne({name: req.body.name}, function (err, object) {
                if (err || !object) {
                    Api.callback(res, {'message': 'User not found'});
                } else {
                   
                    object.comparePassword(req.body.password, function (err, isMatch) {
                        
                        if (isMatch && !err) {
                            let token = jwt.sign(object.publicFields(), Api.config.secret, {
                                expiresIn: 36000
                            });
                            Api.callback(res, false, {'token': 'JWT ' + token});
                        } else {
                           
                            Api.callback(res, {'message': 'User password did not match'});
                        }

                    });
                }

            });
        });

router.route('/user')
        .post(function (req, res) {
            let user = new User();
            if(!req.body.name || !req.body.password){
                Api.callback(res, {'message': 'empty name or password'}, {});
            }
            user.name = req.body.name;
            user.password = req.body.password;

            user.save(err => Api.callback(res, err, user.publicFields()));
        })
        .get((req, res) => Api.queryBuilder(User, res, req));

router.route('/user/:id')
        .get(function (req, res) {
            User.findById(req.params.id, (err, object) => Api.callback(res, err, object));
        })
        .put(passport.authenticate('jwt', {session: false}),function (req, res) {
            User.findById(req.params.id, (err, object) => {
                if (err || req.user._id !== req.params.id) {
                    Api.callback(res, err || {'message': 'user not found'}, null);
                }
                object.name = req.body.name;
                object.password = req.body.password;
                object.save(err => Api.callback(res, err, object));
            });
        })
        .delete(function (req, res) {
            /**
             * Nie pozwalam nigdy usunąć user-a można co najwyżej zrobić go nieaktywnym
             */
            err = {
                'message': `You can't delete user. `
            };
            Api.callback(res, err, {});
        //    User.remove({'_id': req.params.id}, (err, object) => Api.callback(res, err, {'message': 'Deleted'}));
        });


app.use('/', router);

app.listen(port);
console.log(`OverCloud API using ${port} port`);

