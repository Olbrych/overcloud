"use strict";
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

var User = require('./models/User');
var Api = require('./api');

module.exports = function(passport){
    let options = {
      //  jwtFromRequest: ExtractJwt.fromBodyField(),
        jwtFromRequest: ExtractJwt.fromAuthHeader(),
        secretOrKey: Api.config.secret
    };
    
  
    passport.use(new JwtStrategy(options, function(jwt_payload, done){
      
        User.findOne({_id: jwt_payload._id},function(err, object){
           console.log(jwt_payload);
           console.log(object);
           if(err){
               return done(err,false);
           } 
           if(object){
              
               done(null,object.publicFields());
           }else{
               done(null,false);
           }
           
        });
    }));
};
