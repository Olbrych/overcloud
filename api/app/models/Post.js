"use strict";
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    text: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    user: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
    
});


PostSchema.statics.selectObject = function(query) {
    return this.find(query).populate('user','-password');
};

PostSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Post',PostSchema);
