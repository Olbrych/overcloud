"use strict";


var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
//        select: false // nie może być tej funkcji bo wtedy nie można użyc this
    }
});


UserSchema.plugin(uniqueValidator);

UserSchema.pre('save',function(next){
   var user = this;
   if(this.isModified('password') || this.isNew){
       bcrypt.genSalt(10,function(err,salt){
          if(err){
              return next(err);
          } 
          bcrypt.hash(user.password,salt,function(err, hash){
              if(err){
                  return next(err);                  
              }
              user.password = hash;
              next();
          });
       });
   }else{
       return next();
   }
});

UserSchema.methods.publicFields = function () {
    return {
        "name": this.name,
        "_id": this._id
    };
};

UserSchema.statics.selectObject = function(query) {
    return this.find(query).select('-password');
};

UserSchema.selectObject = function(query) {
    return this.find(query).select('-password');
};


UserSchema.methods.comparePassword = function(password,callback){
   
    bcrypt.compare(password,this.password,function(err,isMatch){
       console.log(isMatch);
       if(err){
           callback(err,false);
       }
       callback(null,isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);
