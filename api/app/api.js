"use strict";
var _ = require('lodash');

module.exports = {
    callback: function (res, err, result) {
        if (err) {
            res.json({
                "status": 0,
                "error": err
            });
        }
        res.json({
            "status": 1,
            "result": result
        });
    },
    queryBuilder: function (object, res, req) {

        if (_.isUndefined(req.query)) {
            req.query = {};
        }else{
            req.query = JSON.parse(req.query);
        }
       
        if (typeof object.selectObject === 'function') {
           
            var result = object.selectObject(req.query);
        } else {
            var result = object.find(req.query);
        }
        if (!_.isUndefined(req.limit)) {
            console.log(req.limit);
            result.limit(parseInt(req.limit));
        }
        if (!_.isUndefined(req.sort)) {
//            console.log(JSON.parse(req.sort));
            result.sort(JSON.parse(req.sort));
        }


        result.exec((err, values) => {
            this.callback(res, err, values);
        })
    },
    findOne: function (object, req, res) {
        object.findById(req.params.id, (err, result) => this.callback(res, err, result));
    },
    config: {
        secret: 'zxczxczxc',
        mongodb: 'mongodb://127.0.0.1:27017/overcloud'
    }
};
